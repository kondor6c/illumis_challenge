FROM kubler/python3


WORKDIR /home/python
ADD . .
RUN chown python:python -R /home/python
USER python

RUN pip install --user -r $HOME/requirements.txt && \
    pip install --user gunicorn
ENV PATH=$PATH:./.local/bin/

EXPOSE 8000
CMD ["gunicorn", "app:app"  ]
