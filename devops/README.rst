Guide
=====
Here is a guide to running this project and evaluation.

python
------
run ``pip install tox``

containers
----------
Attempts have been made to avoid using docker as the name of containers.

Build
*****
try running ``docker build -t foo/bar:ver Containerfile``

Kubernetes
----------
to generate a deployment ``podman generate kube foo-bar > foo-bar.yml``
