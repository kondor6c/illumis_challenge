#!/usr/bin/env bash

export DB_NAME="${DB_NAME:-test}"
DB_PATH=${DB_PATH:-/data}
mkdir -p ${DB_PATH}

for path in \
    "$DB_PATH/WiredTiger" \
    "$DB_PATH/journal" \
    "$DB_PATH/local.0" \
    "$DB_PATH/storage.bson" do
  if [[ -e "$path" ]]; then
    break
  fi

  if [ "$ROOT_USER" ] || [ "$ROOT_PASS" ]; then
    rootAuthDatabase='admin'
  mongo "$rootAuthDatabase" <<-EOJS
	db.createUser({
		user: "$ROOT_USER",
		pwd: "$ROOT_PASS",
		roles: [ { role: 'root', db: "$rootAuthDatabase") } ]
	})
EOJS
  mongo ${DB_NAME} <<-EOJS
	db.createUser({
		user: "$DB_USER",
		pwd: "$DB_PASS",
		roles: [ { role: 'readWrite', db: "${DB_NAME}") } ]
	})
EOJS
  mongoimport --db ${DB_NAME} --collection illumis --file ${DB_IMPORT}
  fi
  if [[ ! -z $CONFIG_FILE ]]; then
    DB_OPTS="${DB_OPTS} --config ${CONFIG_FILE}"
  fi

done

mongo ${DB_OPTS}
